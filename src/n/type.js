//@flow strict

import {I, O} from './n'
import {OO, OI, IO, II} from './nn'
import {OOO, OOI, OIO, OII, IOO, IOI, IIO, III} from './nnn'

export type N = I | O
export type NN = OO | OI | IO | II
export type NNN = OOO | OOI | OIO | OII | IOO | IOI | IIO | III

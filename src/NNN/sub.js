//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import * as add from './add'
import * as invert from './invert'
import * as Case from '../n/nnn'

import {getThrow} from '../throw'
import {NNN, Type} from '../n'

declare export function N__(nnn: NNN.OOO): NNN.OOO
declare export function N__(nnn: NNN.OOI): NNN.OOI
declare export function N__(nnn: NNN.OIO): NNN.OIO
declare export function N__(nnn: NNN.OII): NNN.OII
declare export function N__(nnn: NNN.IOO): NNN.OOO
declare export function N__(nnn: NNN.IOI): NNN.OOI
declare export function N__(nnn: NNN.IIO): NNN.OIO
declare export function N__(nnn: NNN.III): NNN.OII
export function N__(nnn: Type.NNN) {
  return invert.N__(add.N__(invert.N__(nnn)))
}

declare export function _N_(nnn: NNN.OOO): NNN.OOO
declare export function _N_(nnn: NNN.OOI): NNN.OOI
declare export function _N_(nnn: NNN.OIO): NNN.OOO
declare export function _N_(nnn: NNN.OII): NNN.OOI
declare export function _N_(nnn: NNN.IOO): NNN.IOO
declare export function _N_(nnn: NNN.IOI): NNN.IOI
declare export function _N_(nnn: NNN.IIO): NNN.IOO
declare export function _N_(nnn: NNN.III): NNN.IOI
export function _N_(nnn: Type.NNN) {
  return invert._N_(add._N_(invert._N_(nnn)))
}

declare export function __N(nnn: NNN.OOO): NNN.OOO
declare export function __N(nnn: NNN.OOI): NNN.OOO
declare export function __N(nnn: NNN.OIO): NNN.OIO
declare export function __N(nnn: NNN.OII): NNN.OIO
declare export function __N(nnn: NNN.IOO): NNN.IOO
declare export function __N(nnn: NNN.IOI): NNN.IOO
declare export function __N(nnn: NNN.IIO): NNN.IIO
declare export function __N(nnn: NNN.III): NNN.IIO
export function __N(nnn: Type.NNN) {
  return invert.__N(add.__N(invert.__N(nnn)))
}

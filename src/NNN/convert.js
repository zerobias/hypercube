//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {NNN, Type} from '../n'

declare export function toBitmask(nnn: NNN.OOO): 0
declare export function toBitmask(nnn: NNN.OOI): 1
declare export function toBitmask(nnn: NNN.OIO): 2
declare export function toBitmask(nnn: NNN.OII): 3
declare export function toBitmask(nnn: NNN.IOO): 4
declare export function toBitmask(nnn: NNN.IOI): 5
declare export function toBitmask(nnn: NNN.IIO): 6
declare export function toBitmask(nnn: NNN.III): 7
export function toBitmask(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return 0
  if (nnn instanceof NNN.OOI) return 1
  if (nnn instanceof NNN.OIO) return 2
  if (nnn instanceof NNN.OII) return 3
  if (nnn instanceof NNN.IOO) return 4
  if (nnn instanceof NNN.IOI) return 5
  if (nnn instanceof NNN.IIO) return 6
  if (nnn instanceof NNN.III) return 7
  /*::;(nnn: empty)*/
  throw getThrow()
}

export function toFlags(nnn: Type.NNN): [boolean, boolean, boolean] {
  if (nnn instanceof NNN.OOO) return [false, false, false]
  if (nnn instanceof NNN.OOI) return [false, false, true]
  if (nnn instanceof NNN.OIO) return [false, true, false]
  if (nnn instanceof NNN.OII) return [false, true, true]
  if (nnn instanceof NNN.IOO) return [true, false, false]
  if (nnn instanceof NNN.IOI) return [true, false, true]
  if (nnn instanceof NNN.IIO) return [true, true, false]
  if (nnn instanceof NNN.III) return [true, true, true]
  /*::;(nnn: empty)*/
  throw getThrow()
}

export function fromFlags([a, b, c]: [boolean, boolean, boolean]): Type.NNN {
  if (!a && !b && !c) return new NNN.OOO()
  if (!a && !b && c) return new NNN.OOI()
  if (!a && b && !c) return new NNN.OIO()
  if (!a && b && c) return new NNN.OII()
  if (a && !b && !c) return new NNN.IOO()
  if (a && !b && c) return new NNN.IOI()
  if (a && b && !c) return new NNN.IIO()
  if (a && b && c) return new NNN.III()
  throw getThrow()
}

export function fromBitmask(nnn: number) {
  switch (nnn) {
    case 0:
      return new NNN.OOO()
    case 1:
      return new NNN.OOI()
    case 2:
      return new NNN.OIO()
    case 3:
      return new NNN.OII()
    case 4:
      return new NNN.IOO()
    case 5:
      return new NNN.IOI()
    case 6:
      return new NNN.IIO()
    case 7:
      return new NNN.III()
    default: {
      throw getThrow()
    }
  }
}

export function fromBitmaskClamp(nnn: number) {
  if (nnn >= 0 && nnn <= 7) return fromBitmask(nnn)
  if (nnn > 7) return fromBitmask(7)
  return fromBitmask(0)
}

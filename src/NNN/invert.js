//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {NNN, Type} from '../n'

declare export function __N(nnn: NNN.OOO): NNN.OOI
declare export function __N(nnn: NNN.OOI): NNN.OOO
declare export function __N(nnn: NNN.OIO): NNN.OII
declare export function __N(nnn: NNN.OII): NNN.OIO
declare export function __N(nnn: NNN.IOO): NNN.IOI
declare export function __N(nnn: NNN.IOI): NNN.IOO
declare export function __N(nnn: NNN.IIO): NNN.III
declare export function __N(nnn: NNN.III): NNN.IIO
export function __N(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NNN.OOI()
  if (nnn instanceof NNN.OOI) return new NNN.OOO()
  if (nnn instanceof NNN.OIO) return new NNN.OII()
  if (nnn instanceof NNN.OII) return new NNN.OIO()
  if (nnn instanceof NNN.IOO) return new NNN.IOI()
  if (nnn instanceof NNN.IOI) return new NNN.IOO()
  if (nnn instanceof NNN.IIO) return new NNN.III()
  if (nnn instanceof NNN.III) return new NNN.IIO()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function _N_(nnn: NNN.OOO): NNN.OIO
declare export function _N_(nnn: NNN.OOI): NNN.OII
declare export function _N_(nnn: NNN.OIO): NNN.OOO
declare export function _N_(nnn: NNN.OII): NNN.OOI
declare export function _N_(nnn: NNN.IOO): NNN.IIO
declare export function _N_(nnn: NNN.IOI): NNN.III
declare export function _N_(nnn: NNN.IIO): NNN.IOO
declare export function _N_(nnn: NNN.III): NNN.IOI
export function _N_(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NNN.OIO()
  if (nnn instanceof NNN.OOI) return new NNN.OII()
  if (nnn instanceof NNN.OIO) return new NNN.OOO()
  if (nnn instanceof NNN.OII) return new NNN.OOI()
  if (nnn instanceof NNN.IOO) return new NNN.IIO()
  if (nnn instanceof NNN.IOI) return new NNN.III()
  if (nnn instanceof NNN.IIO) return new NNN.IOO()
  if (nnn instanceof NNN.III) return new NNN.IOI()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function N__(nnn: NNN.OOO): NNN.IOO
declare export function N__(nnn: NNN.OOI): NNN.IOI
declare export function N__(nnn: NNN.OIO): NNN.IIO
declare export function N__(nnn: NNN.OII): NNN.III
declare export function N__(nnn: NNN.IOO): NNN.OOO
declare export function N__(nnn: NNN.IOI): NNN.OOI
declare export function N__(nnn: NNN.IIO): NNN.OIO
declare export function N__(nnn: NNN.III): NNN.OII
export function N__(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NNN.IOO()
  if (nnn instanceof NNN.OOI) return new NNN.IOI()
  if (nnn instanceof NNN.OIO) return new NNN.IIO()
  if (nnn instanceof NNN.OII) return new NNN.III()
  if (nnn instanceof NNN.IOO) return new NNN.OOO()
  if (nnn instanceof NNN.IOI) return new NNN.OOI()
  if (nnn instanceof NNN.IIO) return new NNN.OIO()
  if (nnn instanceof NNN.III) return new NNN.OII()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function all(nnn: NNN.OOO): NNN.III
declare export function all(nnn: NNN.OOI): NNN.IIO
declare export function all(nnn: NNN.OIO): NNN.IOI
declare export function all(nnn: NNN.OII): NNN.IOO
declare export function all(nnn: NNN.IOO): NNN.OII
declare export function all(nnn: NNN.IOI): NNN.OIO
declare export function all(nnn: NNN.IIO): NNN.OOI
declare export function all(nnn: NNN.III): NNN.OOO
export function all(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NNN.III()
  if (nnn instanceof NNN.OOI) return new NNN.IIO()
  if (nnn instanceof NNN.OIO) return new NNN.IOI()
  if (nnn instanceof NNN.OII) return new NNN.IOO()
  if (nnn instanceof NNN.IOO) return new NNN.OII()
  if (nnn instanceof NNN.IOI) return new NNN.OIO()
  if (nnn instanceof NNN.IIO) return new NNN.OOI()
  if (nnn instanceof NNN.III) return new NNN.OOO()
  /*::;(nnn: empty)*/
  throw getThrow()
}

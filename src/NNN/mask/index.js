//@flow strict

import * as one from './mask1'
import * as two from './mask2'

export {one, two}

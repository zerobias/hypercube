//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../../throw'
import {NNN, N, Type} from '../../n'

declare export function __N(nnn: NNN.OOO): N.O
declare export function __N(nnn: NNN.OOI): N.I
declare export function __N(nnn: NNN.OIO): N.O
declare export function __N(nnn: NNN.OII): N.I
declare export function __N(nnn: NNN.IOO): N.O
declare export function __N(nnn: NNN.IOI): N.I
declare export function __N(nnn: NNN.IIO): N.O
declare export function __N(nnn: NNN.III): N.I
export function __N(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new N.O()
  if (nnn instanceof NNN.OOI) return new N.I()
  if (nnn instanceof NNN.OIO) return new N.O()
  if (nnn instanceof NNN.OII) return new N.I()
  if (nnn instanceof NNN.IOO) return new N.O()
  if (nnn instanceof NNN.IOI) return new N.I()
  if (nnn instanceof NNN.IIO) return new N.O()
  if (nnn instanceof NNN.III) return new N.I()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function _N_(nnn: NNN.OOO): N.O
declare export function _N_(nnn: NNN.OOI): N.O
declare export function _N_(nnn: NNN.OIO): N.I
declare export function _N_(nnn: NNN.OII): N.I
declare export function _N_(nnn: NNN.IOO): N.O
declare export function _N_(nnn: NNN.IOI): N.O
declare export function _N_(nnn: NNN.IIO): N.I
declare export function _N_(nnn: NNN.III): N.I
export function _N_(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new N.O()
  if (nnn instanceof NNN.OOI) return new N.O()
  if (nnn instanceof NNN.OIO) return new N.I()
  if (nnn instanceof NNN.OII) return new N.I()
  if (nnn instanceof NNN.IOO) return new N.O()
  if (nnn instanceof NNN.IOI) return new N.O()
  if (nnn instanceof NNN.IIO) return new N.I()
  if (nnn instanceof NNN.III) return new N.I()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function N__(nnn: NNN.OOO): N.O
declare export function N__(nnn: NNN.OOI): N.O
declare export function N__(nnn: NNN.OIO): N.O
declare export function N__(nnn: NNN.OII): N.O
declare export function N__(nnn: NNN.IOO): N.I
declare export function N__(nnn: NNN.IOI): N.I
declare export function N__(nnn: NNN.IIO): N.I
declare export function N__(nnn: NNN.III): N.I
export function N__(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new N.O()
  if (nnn instanceof NNN.OOI) return new N.O()
  if (nnn instanceof NNN.OIO) return new N.O()
  if (nnn instanceof NNN.OII) return new N.O()
  if (nnn instanceof NNN.IOO) return new N.I()
  if (nnn instanceof NNN.IOI) return new N.I()
  if (nnn instanceof NNN.IIO) return new N.I()
  if (nnn instanceof NNN.III) return new N.I()
  /*::;(nnn: empty)*/
  throw getThrow()
}

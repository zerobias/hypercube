//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../../throw'
import {NNN, NN, Type} from '../../n'

declare export function _NN(nnn: NNN.OOO): NN.OO
declare export function _NN(nnn: NNN.OOI): NN.OI
declare export function _NN(nnn: NNN.OIO): NN.IO
declare export function _NN(nnn: NNN.OII): NN.II
declare export function _NN(nnn: NNN.IOO): NN.OO
declare export function _NN(nnn: NNN.IOI): NN.OI
declare export function _NN(nnn: NNN.IIO): NN.IO
declare export function _NN(nnn: NNN.III): NN.II
export function _NN(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NN.OO()
  if (nnn instanceof NNN.OOI) return new NN.OI()
  if (nnn instanceof NNN.OIO) return new NN.IO()
  if (nnn instanceof NNN.OII) return new NN.II()
  if (nnn instanceof NNN.IOO) return new NN.OO()
  if (nnn instanceof NNN.IOI) return new NN.OI()
  if (nnn instanceof NNN.IIO) return new NN.IO()
  if (nnn instanceof NNN.III) return new NN.II()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function N_N(nnn: NNN.OOO): NN.OO
declare export function N_N(nnn: NNN.OOI): NN.OI
declare export function N_N(nnn: NNN.OIO): NN.OO
declare export function N_N(nnn: NNN.OII): NN.OI
declare export function N_N(nnn: NNN.IOO): NN.IO
declare export function N_N(nnn: NNN.IOI): NN.II
declare export function N_N(nnn: NNN.IIO): NN.IO
declare export function N_N(nnn: NNN.III): NN.II
export function N_N(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NN.OO()
  if (nnn instanceof NNN.OOI) return new NN.OI()
  if (nnn instanceof NNN.OIO) return new NN.OO()
  if (nnn instanceof NNN.OII) return new NN.OI()
  if (nnn instanceof NNN.IOO) return new NN.IO()
  if (nnn instanceof NNN.IOI) return new NN.II()
  if (nnn instanceof NNN.IIO) return new NN.IO()
  if (nnn instanceof NNN.III) return new NN.II()
  /*::;(nnn: empty)*/
  throw getThrow()
}

declare export function NN_(nnn: NNN.OOO): NN.OO
declare export function NN_(nnn: NNN.OOI): NN.OO
declare export function NN_(nnn: NNN.OIO): NN.OI
declare export function NN_(nnn: NNN.OII): NN.OI
declare export function NN_(nnn: NNN.IOO): NN.IO
declare export function NN_(nnn: NNN.IOI): NN.IO
declare export function NN_(nnn: NNN.IIO): NN.II
declare export function NN_(nnn: NNN.III): NN.II
export function NN_(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NN.OO()
  if (nnn instanceof NNN.OOI) return new NN.OO()
  if (nnn instanceof NNN.OIO) return new NN.OI()
  if (nnn instanceof NNN.OII) return new NN.OI()
  if (nnn instanceof NNN.IOO) return new NN.IO()
  if (nnn instanceof NNN.IOI) return new NN.IO()
  if (nnn instanceof NNN.IIO) return new NN.II()
  if (nnn instanceof NNN.III) return new NN.II()
  /*::;(nnn: empty)*/
  throw getThrow()
}

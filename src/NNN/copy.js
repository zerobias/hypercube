//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {NNN, Type} from '../n'

declare export function copy(nnn: NNN.OOO): NNN.OOO
declare export function copy(nnn: NNN.OOI): NNN.OOI
declare export function copy(nnn: NNN.OIO): NNN.OIO
declare export function copy(nnn: NNN.OII): NNN.OII
declare export function copy(nnn: NNN.IOO): NNN.IOO
declare export function copy(nnn: NNN.IOI): NNN.IOI
declare export function copy(nnn: NNN.IIO): NNN.IIO
declare export function copy(nnn: NNN.III): NNN.III
export function copy(nnn: Type.NNN) {
  if (nnn instanceof NNN.OOO) return new NNN.OOO()
  if (nnn instanceof NNN.OOI) return new NNN.OOI()
  if (nnn instanceof NNN.OIO) return new NNN.OIO()
  if (nnn instanceof NNN.OII) return new NNN.OII()
  if (nnn instanceof NNN.IOO) return new NNN.IOO()
  if (nnn instanceof NNN.IOI) return new NNN.IOI()
  if (nnn instanceof NNN.IIO) return new NNN.IIO()
  if (nnn instanceof NNN.III) return new NNN.III()
  /*::;(nnn: empty)*/
  throw getThrow()
}

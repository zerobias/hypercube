//@flow strict

import * as N from './one-bit'
import * as NN from './two-bits'
import * as NNN from './three-bits'

export type {OneBit, TwoBits, ThreeBits} from './type'

export {N, NN, NNN}

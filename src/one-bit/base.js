//@flow strict

export {I, O} from '../base/one-bit'
export type {OneBit} from '../base/type'

//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function invert(n: Bit.O): Bit.I
declare export function invert(n: Bit.I): Bit.O
export function invert(n: Bit.OneBit) {
  if (n instanceof Bit.O) return new Bit.I()
  if (n instanceof Bit.I) return new Bit.O()
  /*::;(n: empty)*/
  throw getThrow()
}

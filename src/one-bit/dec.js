//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function dec(n: Bit.O): Bit.O
declare export function dec(n: Bit.I): Bit.O
export function dec(n: Bit.OneBit) {
  if (n instanceof Bit.O) return new Bit.O()
  if (n instanceof Bit.I) return new Bit.O()
  /*::;(n: empty)*/
  throw getThrow()
}

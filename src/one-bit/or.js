//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function or(a: Bit.O, b: Bit.O): Bit.O
declare export function or(a: Bit.O, b: Bit.I): Bit.I
declare export function or(a: Bit.I, b: Bit.O): Bit.I
declare export function or(a: Bit.I, b: Bit.I): Bit.I
export function or(a: Bit.OneBit, b: Bit.OneBit) {
  if (a instanceof Bit.O) {
    if (b instanceof Bit.O) return new Bit.O()
    if (b instanceof Bit.I) return new Bit.I()
    /*::;(b: empty)*/
    throw getThrow()
  }
  if (a instanceof Bit.I) return new Bit.I()
  /*::;(a: empty)*/
  throw getThrow()
}

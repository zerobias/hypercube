//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function copy(n: Bit.O): Bit.O
declare export function copy(n: Bit.I): Bit.I
export function copy(n: Bit.OneBit) {
  if (n instanceof Bit.O) return new Bit.O()
  if (n instanceof Bit.I) return new Bit.I()
  /*::;(n: empty)*/
  throw getThrow()
}

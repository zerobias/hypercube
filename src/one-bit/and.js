//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function and(a: Bit.O, b: Bit.O): Bit.O
declare export function and(a: Bit.O, b: Bit.I): Bit.O
declare export function and(a: Bit.I, b: Bit.O): Bit.O
declare export function and(a: Bit.I, b: Bit.I): Bit.I
export function and(a: Bit.OneBit, b: Bit.OneBit) {
  if (a instanceof Bit.O) return new Bit.O()
  if (a instanceof Bit.I) {
    if (b instanceof Bit.O) return new Bit.O()
    if (b instanceof Bit.I) return new Bit.I()
    /*::;(b: empty)*/
    throw getThrow()
  }
  /*::;(a: empty)*/
  throw getThrow()
}

//@flow strict

import * as Bit from '../base'
import {getThrow} from '../../throw'

declare export function toNumber(n: Bit.O): 0
declare export function toNumber(n: Bit.I): 1
export function toNumber(n: Bit.OneBit) {
  if (n instanceof Bit.O) return 0
  if (n instanceof Bit.I) return 1
  /*::;(n: empty)*/
  throw getThrow()
}

export function fromNumber(n: number) {
  if (n >= 1) return new Bit.I()
  return new Bit.O()
}

declare export function fromNumberStrict(n: 0): Bit.O
declare export function fromNumberStrict(n: 1): Bit.I
export function fromNumberStrict(n: number) {
  switch (n) {
    case 0:
      return new Bit.O()
    case 1:
      return new Bit.I()
    default: {
      throw getThrow()
    }
  }
}

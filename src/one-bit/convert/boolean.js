//@flow strict

import * as Bit from '../base'
import {getThrow} from '../../throw'

declare export function toBoolean(n: Bit.O): false
declare export function toBoolean(n: Bit.I): true
export function toBoolean(n: Bit.OneBit) {
  if (n instanceof Bit.O) return false
  if (n instanceof Bit.I) return true
  /*::;(n: empty)*/
  throw getThrow()
}

export function fromBoolean(b: boolean) {
  if (b) return new Bit.I()
  return new Bit.O()
}

declare export function fromBooleanStrict(b: false): Bit.O
declare export function fromBooleanStrict(b: true): Bit.I
export function fromBooleanStrict(b: boolean) {
  switch (b) {
    case false:
      return new Bit.O()
    case true:
      return new Bit.I()
    default: {
      throw getThrow()
    }
  }
}

//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function eq(a: Bit.O, b: Bit.O): Bit.I
declare export function eq(a: Bit.O, b: Bit.I): Bit.O
declare export function eq(a: Bit.I, b: Bit.O): Bit.O
declare export function eq(a: Bit.I, b: Bit.I): Bit.I
export function eq(a: Bit.OneBit, b: Bit.OneBit) {
  if (a instanceof Bit.O) {
    if (b instanceof Bit.O) return new Bit.I()
    if (b instanceof Bit.I) return new Bit.O()
    /*::;(b: empty)*/
    throw getThrow()
  }
  if (a instanceof Bit.I) {
    if (b instanceof Bit.O) return new Bit.O()
    if (b instanceof Bit.I) return new Bit.I()
    /*::;(b: empty)*/
    throw getThrow()
  }
  /*::;(a: empty)*/
  throw getThrow()
}

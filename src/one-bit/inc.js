//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function inc(n: Bit.O): Bit.I
declare export function inc(n: Bit.I): Bit.I
export function inc(n: Bit.OneBit) {
  if (n instanceof Bit.O) return new Bit.I()
  if (n instanceof Bit.I) return new Bit.I()
  /*::;(n: empty)*/
  throw getThrow()
}

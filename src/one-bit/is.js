//@flow strict

import * as Bit from './base'

export function isBit(n: mixed): boolean %checks {
  return n instanceof Bit.O || n instanceof Bit.I
}

export function isBit0(n: mixed): boolean %checks {
  return n instanceof Bit.O
}

export function isBit1(n: mixed): boolean %checks {
  return n instanceof Bit.I
}

//@flow strict

import * as Bit from './base'
import * as TwoBits from '../base/two-bits'
import {getThrow} from '../throw'

declare export function sum(a: Bit.O, b: Bit.O): TwoBits.OO
declare export function sum(a: Bit.O, b: Bit.I): TwoBits.OI
declare export function sum(a: Bit.I, b: Bit.O): TwoBits.OI
declare export function sum(a: Bit.I, b: Bit.I): TwoBits.II
export function sum(a: Bit.OneBit, b: Bit.OneBit) {
  if (a instanceof Bit.O) {
    if (b instanceof Bit.O) return new TwoBits.OO()
    if (b instanceof Bit.I) return new TwoBits.OI()
    /*::;(b: empty)*/
    throw getThrow()
  }
  if (a instanceof Bit.I) {
    if (b instanceof Bit.O) return new TwoBits.OI()
    if (b instanceof Bit.I) return new TwoBits.II()
    /*::;(b: empty)*/
    throw getThrow()
  }
  /*::;(a: empty)*/
  throw getThrow()
}

//@flow strict

import * as Bit from './base'
import {getThrow} from '../throw'

declare export function compare(a: Bit.O, b: Bit.O): 0
declare export function compare(a: Bit.O, b: Bit.I): -1
declare export function compare(a: Bit.I, b: Bit.O): 1
declare export function compare(a: Bit.I, b: Bit.I): 0
export function compare(a: Bit.OneBit, b: Bit.OneBit) {
  if (a instanceof Bit.O) {
    if (b instanceof Bit.O) return 0
    if (b instanceof Bit.I) return -1
    /*::;(b: empty)*/
    throw getThrow()
  }
  if (a instanceof Bit.I) {
    if (b instanceof Bit.O) return 1
    if (b instanceof Bit.I) return 0
    /*::;(b: empty)*/
    throw getThrow()
  }
  /*::;(a: empty)*/
  throw getThrow()
}

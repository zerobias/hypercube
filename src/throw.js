//@flow strict

export function getThrow() {
  return new TypeError('Case mismatch')
}

//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {NNN, NN, Type} from '../n'

declare export function addDigit(nn: NN.OO): NNN.OOO
declare export function addDigit(nn: NN.OI): NNN.OOI
declare export function addDigit(nn: NN.IO): NNN.OIO
declare export function addDigit(nn: NN.II): NNN.OII
export function addDigit(nn: Type.NN) {
  if (nn instanceof NN.OO) return new NNN.OOO()
  if (nn instanceof NN.OI) return new NNN.OOI()
  if (nn instanceof NN.IO) return new NNN.OIO()
  if (nn instanceof NN.II) return new NNN.OII()
  /*::;(nn: empty)*/
  throw getThrow()
}

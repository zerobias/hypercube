//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {NN, N, Type} from '../n'

declare export function N_(nn: NN.OO): N.O
declare export function N_(nn: NN.OI): N.O
declare export function N_(nn: NN.IO): N.I
declare export function N_(nn: NN.II): N.I
export function N_(nn: Type.NN) {
  if (nn instanceof NN.OO) return new N.O()
  if (nn instanceof NN.OI) return new N.O()
  if (nn instanceof NN.IO) return new N.I()
  if (nn instanceof NN.II) return new N.I()
  /*::;(nn: empty)*/
  throw getThrow()
}

declare export function _N(nn: NN.OO): N.O
declare export function _N(nn: NN.OI): N.I
declare export function _N(nn: NN.IO): N.O
declare export function _N(nn: NN.II): N.I
export function _N(nn: Type.NN) {
  if (nn instanceof NN.OO) return new N.O()
  if (nn instanceof NN.OI) return new N.I()
  if (nn instanceof NN.IO) return new N.O()
  if (nn instanceof NN.II) return new N.I()
  /*::;(nn: empty)*/
  throw getThrow()
}

//@flow strict
/* eslint-disable no-unused-vars, no-duplicate-imports */

import {getThrow} from '../throw'
import {N, Type} from '../n'
import * as NN from '../NN'
import * as NNN from '../NNN'

declare export function concatN(o: N.O, i: N.O): NN.Case.OO
declare export function concatN(o: N.O, i: N.I): NN.Case.OI
declare export function concatN(o: N.I, i: N.O): NN.Case.IO
declare export function concatN(o: N.I, i: N.I): NN.Case.II
export function concatN(o: Type.N, i: Type.N) {
  if (o instanceof N.O) return concatO_N(i)
  if (o instanceof N.I) return concatI_N(i)
  /*::;(o: empty)*/
  throw getThrow()
}

declare export function concatNN(o: N.O, i: NN.Case.OO): NNN.Case.OOO
declare export function concatNN(o: N.O, i: NN.Case.OI): NNN.Case.OOI
declare export function concatNN(o: N.O, i: NN.Case.IO): NNN.Case.OIO
declare export function concatNN(o: N.O, i: NN.Case.II): NNN.Case.OII
declare export function concatNN(o: N.I, i: NN.Case.OO): NNN.Case.IOO
declare export function concatNN(o: N.I, i: NN.Case.OI): NNN.Case.IOI
declare export function concatNN(o: N.I, i: NN.Case.IO): NNN.Case.IIO
declare export function concatNN(o: N.I, i: NN.Case.II): NNN.Case.III
export function concatNN(o: Type.N, i: NN.Type) {
  if (o instanceof N.O) return NN.addDigit(i)
  if (o instanceof N.I) return NNN.invert.N__(NN.addDigit(i))
  /*::;(o: empty)*/
  throw getThrow()
}

declare function concatO_N(i: N.O): NN.Case.OO
declare function concatO_N(i: N.I): NN.Case.OI
function concatO_N(i: Type.N) {
  if (i instanceof N.O) return new NN.Case.OO()
  if (i instanceof N.I) return new NN.Case.OI()
  /*::;(i: empty)*/
  throw getThrow()
}

declare function concatI_N(i: N.O): NN.Case.IO
declare function concatI_N(i: N.I): NN.Case.II
function concatI_N(i: Type.N) {
  if (i instanceof N.O) return new NN.Case.IO()
  if (i instanceof N.I) return new NN.Case.II()
  /*::;(i: empty)*/
  throw getThrow()
}
